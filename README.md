## PAT (Personal Access Tokens) sagatavošana GitLab
- [ ] Atver (Need to add repo link) un izveido personal access token ar ko lejuplādēsi kodu uz EC2 instances
  * Token name - aws
  * Expiration date - atstāj tukšu
  * Select scopes - iezīmē api un read_repository
  Spied "Create personal access tooken" un saglabā izvedioto token. Atstājot šo lapu, otro reizi vairs nevarēsi to redzēt

## Darba vides sagatavošana izmantojot Cloudformation un pieslēgšanās instancei
- [ ] Lejuplādē `ansible-stack.yaml`
- [ ] Atver AWS konsoli https://952122846739.signin.aws.amazon.com/console un ieej savā kontā:
- [ ] Pārliecinies vai augšējā labā stūrī reģions izvēlēts *Ireland* (Europe (Ireland) eu-west-1). Ja nē, nomaini to uz *Ireland*
- [ ] Atver Cloudformation konsoli meklējot resursu *CloudFormation* un izvēloties *Create Stack* vai atverot https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks/create/template, kas atver formu pirmā CF stack izveidei.
  1. *Specify template* solis - pie *Prepare template* izvēlies *Template is ready*, pie *Specify template* - *Upload a template file* -> Choose file un augšuplādē `ansible-stack.yaml` failu -> Next
  1. *Specify stack details*
      - Stack name - tavsVards-ansible-stack
      - InstanceTypeParameter - atstāj noklusējuma vērtību *t3.small*
      - OwnerName - tavsVards bez atstarpēm, mīkstinājuma un garumzīmēm
    Spied *Next*
  1. *Configure stack options*
      - Pie Tags pievienojam jaunu tag
      - Key: School
      - Value: RTU2022
      - Pašā apkašā spiežam "Next"
  1. *Review* - pārskati stack detaļas un spied *Create stack*
- [ ] Lai apskatītu progresu, atjauno lapu izmantojot pārlūka vai AWS atjaunošanas pogu labējā augšējā stūrī. Kad statuss ir *CREATE_COMPLETE*, esi izveidojis CloudFormation stack. Spied uz Resources, tad uz *AnsibleInstance* Physical ID, kas jaunā lapā atvērs izveidoto EC2 instanci. Pieslēdzies instancei ar SSM, izvēloties instanci un spiežot "Connect" labējā augšējā stūrī. Tad "Session Manager" tab spied "Connect"

## _Petclinic_ aplikācijas uzstādīšana, izmantojot Ansible, uz EC2 instances
- [ ] Pārvietojies uz home directory un pārliecinies vai ansible ir veiksmīgi ieinstalēts. Tad noklonē kodu no (Insert repo link). Kad tiek jautāts pēc paroles, izmanto izveidoto token. Tad ieej Repo/ansible
  ```sh
  cd ~
  ansible --version
  git clone (GitRepo Clone Link)
  cd Repo/ansible
  ```
- [ ] Izlasi instrukcijas Repo/ansible/README.md un vispirms uzstādi aplikāciju uz EC2 instances:
  ```sh
  export ANSIBLE_HOST_KEY_CHECKING=False
  ansible-galaxy install geerlingguy.java
  ansible-playbook playbook.yml -t ec2
  ```
- [ ] Piekļūsti aplikācijai izmantojot instances publisko IP adresi un 8080 portu (Publisko IP adresi atradīsi meklējot savu instanci https://eu-west-1.console.aws.amazon.com/ec2/home?region=eu-west-1#Instances: *Details* tab zem *Public IPv4 address* vai izvēloties savu stack https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=active&filteringText=&viewNested=true&hideStacks=false *Outputs* tab pie *ApplicationUrlPort8080*)
Petclinic aplikācijas adrese, ja uzstādīta pa taisno uz EC2 instances - http://PubliskaIPadrese:8080/
🎉 Ievieto ekrānuzņēmumu ar Petclinic aplikāciju pie uzdevuma komentārā
- [ ] Izmantojot Ansible tag - clean_ec2_petclinic, apstādini Petclinic procesu
  ```sh
  ansible-playbook playbook.yml -t clean_ec2_petclinic
  ```
## _Petclinic_ aplikācijas uzstādīšana, izmantojot Ansible, uz Docker konteinera
- [ ] Ja vēl nav konts, https://hub.docker.com/, izveido to. Kontu Dockerhub vajadzēs, lai novilktu publisko Petclinic Docker Image
- [ ] Pārliecinies, ka atrodies `repo/ansible`, tad, lai droši ievadītu slepenu informāciju, izveido creds.sh failu ar sekojošu saturu
  ```sh
  pwd
  nano creds.sh
  ```
  ```sh
  export DOCKERHUB_USER="DOCKERHUB_LIETOTAJVARDS"
  export DOCKERHUB_PASS="DOCKERHUB_PAROLE"
  ```
*DOCKERHUB_LIETOTAJVARDS* un *DOCKERHUB_PAROLE* aizstāj ar savu lietotājvārdu un paroli
Saglabā un izej no faila (ctrl+o, enter, ctrl+x)
- [ ] Ielasi informāciju no faila (tādā veidā parole neparādīsies atklātā tekstā, piemēram, izmantojot `history` komandu), tad palaid _ansible-playbook_ komandu, lai izveidotu _Petclinic_ aplikāciju Docker konteinerī:
  ```sh
  source ./creds.sh
  ansible-playbook playbook.yml \
      -t docker \
      -e dockerhub_username=$DOCKERHUB_USER \
      -e dockerhub_password=$DOCKERHUB_PASS
  ```
  Ja vēlies izmantot iepriekšējā laboratorijas darbā izveidoto Docker Image, izmanto komandu:
  ```sh
  ansible-playbook playbook.yml \
      -t docker \
      -e dockerhub_username=$DOCKERHUB_USER \
      -e dockerhub_password=$DOCKERHUB_PASS \
      -e petclinic_docker_image="$DOCKERHUB_USER/petclinic:5.3.0"
  ```
- [ ] Piekļūsti aplikācijai izmantojot instances publisko IP adresi (Publisko IP adresi atradīsi meklējot savu instanci https://eu-west-1.console.aws.amazon.com/ec2/home?region=eu-west-1#Instances: *Details* tab zem *Public IPv4 address* vai izvēloties savu stack https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=active&filteringText=&viewNested=true&hideStacks=false *Outputs* tab pie *ApplicationUrlPort80*)
Petclinic aplikācijas adrese, ja uzstādīta uz Docker Container - http://PubliskaIPadrese/
🎉 Ievieto ekrānuzņēmumu ar Petclinic aplikāciju pie uzdevuma komentārā
- [ ] Izmantojot Ansible tag - clean_docker_petclinic, apstādini Petclinic konteineru
  ```sh
  ansible-playbook playbook.yml -t clean_docker_petclinic
  ```
## Resursu tīrīšana
- [ ] Atver Cloudformation konsoli meklējot resursu *CloudFormation* vai atverot https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=active&filteringText=&viewNested=true&hideStacks=false un atrodi savu stack pēc sava vārda, izvēlies to un spied *Delete* -> *Delete stack*
Lai apskatītu progresu, atjauno lapu izmantojot pārlūka vai AWS atjaunošanas pogu labējā augšējā stūrī. https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=deleted&filteringText=&viewNested=true&hideStacks=false&stackId= meklējot pēc sava vārda vari pārliecināties, ka statuss norāda *DELETE_COMPLETE*
